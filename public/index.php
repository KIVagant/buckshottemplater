<?php
define('REQUEST_MICROTIME', microtime(true));
chdir(dirname(__DIR__));
require 'vendor/autoload.php';
$container = DI\ContainerBuilder::buildDevContainer();

/** @var \Buckshot\Template $templater */
$templater = $container->get(\Buckshot\Template::class);
$templater
    ->use('Count', 10)
    ->use('TextIfEnd', 'and special ending')
    ->use('AnotherVar', 'dynamic')
    ->load('main', 'view/main.tpl')
    ->parse('main')
    ->print('main')
;

echo PHP_EOL . 'Time, ms: '
    . (microtime(true) - REQUEST_MICROTIME);