Count to <var name="Count">:
<loop name="Counting"><var name="Number"> of <var_name="../Count">
<if name="LastNumber">End</if></loop>

-------
Yet another loop from <var name="Count">:
<loop name="Counting"><var name="Number"> of <var_name="../Count"> <if name="LastNumber"><var name="TextIfEnd"> text plus </if><var name="AnotherVar"> variable
</loop>
