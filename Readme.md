## Pseudo-template engine

Created just for fun and for playing with PHP 7.

Look into the ```view/main.tpl```

Notice: nested loops are not supported.

And then run:
```
php public/index.php
```

Model result example:
```
Count to 10:
1 of 10
2 of 10
3 of 10
4 of 10
5 of 10
6 of 10
7 of 10
8 of 10
9 of 10
10 of 10
End

------
Yet another loop from 10:
1 of 10 dynamic variable
2 of 10 dynamic variable
3 of 10 dynamic variable
4 of 10 dynamic variable
5 of 10 dynamic variable
6 of 10 dynamic variable
7 of 10 dynamic variable
8 of 10 dynamic variable
9 of 10 dynamic variable
10 of 10 and special ending text plus dynamic variable
```
