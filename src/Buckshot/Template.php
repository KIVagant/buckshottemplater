<?php
namespace Buckshot;

use Buckshot\Exceptions\RuntimeException;
use Buckshot\Loops\{
    Counting, LoopInterface
};

class Template implements TemplateInterface
{
    const TAG_VAR = 'var';
    const TAG_LOOP = 'loop';
    const VAR_NUMBER = 'Number';
    const NODE_REGEXP = /** @lang RegExp */
        '/<([a-z0-9_]+) name="([a-z0-9_]+)">/i';
    protected $templates = [];
    protected $vars = [];
    protected $loops = [];

    public function __construct(Counting $countingLoop)
    {
        $this->registerLoops($countingLoop);
    }

    /**
     * You can register your own loops
     * @param LoopInterface[] ...$loops
     * @return Template
     */
    public function registerLoops(LoopInterface ...$loops):self
    {
        foreach ($loops as $loop) {
            if (
                isset($this->loops[$loop->getLoopName()])
                && $this->loops[$loop->getLoopName()] !== $loop
            ) {
                throw new RuntimeException('Can not register the loop ' . get_class($loop)
                    . '. The different loop ' . get_class($this->loops[$loop->getLoopName()])
                    . ' with the same name "' . $loop->getLoopName() . '" is already registered.');
            }
            $this->loops[$loop->getLoopName()] = $loop;
        }

        return $this;
    }

    public function use (string $varName, $value):self
    {
        if ($varName === self::VAR_NUMBER) {
            throw new RuntimeException('The "Number" variable name is reserved');
        }
        $this->vars[$varName] = $value;

        return $this;
    }

    public function subLoad(string $tplName, string $text, string $file):self
    {
        $this->templates[$tplName]['file'] = $file;
        $this->templates[$tplName]['text'] = $text;

        return $this;
    }

    public function load(string $tplName, string $file):self
    {
        $this->templates[$tplName]['file'] = $file;
        $this->templates[$tplName]['text'] = file_get_contents($file);
        if (false === $this->templates[$tplName]['text']) {
            throw new RuntimeException('Error while loading template file ' . $file);
        }

        return $this;
    }

    public function parse(string $tplName):self
    {
        $found = [];
        $matches = [];
        preg_match_all(self::NODE_REGEXP, $this->templates[$tplName]['text'], $matches);
        if (!empty($matches[1]) && !empty($matches[2])) {
            foreach ($matches[1] as $index => $nodeType) {
                if (empty($matches[2][$index])) {
                    continue;
                }
                $nodeName = $matches[2][$index];
                switch ($nodeType) {
                    case static::TAG_VAR:
                        if ($nodeName !== self::VAR_NUMBER) {
                            $found[static::TAG_VAR][$nodeName] = ($found[static::TAG_VAR][$nodeName] ?? 1) + 1;
                            $this->templates[$tplName]['text'] = $this->parseVariable($tplName, $nodeName, $found);
                        }
                        break;
                    case self::TAG_LOOP:
                        $found[static::TAG_LOOP][$nodeName] = ($found[static::TAG_LOOP][$nodeName] ?? 1) + 1;
                        $this->templates[$tplName]['text'] = $this->parseLoop($tplName, $nodeName);
                        break;
                }
            }
        }

        return $this;
    }

    public function get(string $tplName):string
    {
        return $this->templates[$tplName]['text'];
    }

    public function print(string $tplName):self
    {
        echo $this->templates[$tplName]['text'];

        return $this;
    }

    protected function parseVariable(string $tplName, string $varName):string
    {
        if (!isset($this->vars[$varName])) {
            throw new RuntimeException('Undefined variable ' . $varName . ' in template "' . $this->templates[$tplName]['file'] . '"');
        }

        $result = preg_replace(
            '/<var name="' . $varName . '">/'
            , $this->vars[$varName]
            , $this->templates[$tplName]['text']
            , 1
        );

        return $result;
    }

    protected function parseLoop(string $tplName, string $nodeName):string
    {
        if (!array_key_exists($nodeName, $this->loops)) {
            throw new RuntimeException('Unregistered loop called in template "' . $this->templates[$tplName]['file'] . '": ' . $nodeName);
        }
        $split = preg_split(
            '/(<loop name="' . $nodeName . '">(|.|\s)+<\/loop>)/'
            , $this->templates[$tplName]['text']
            , 2
            , PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $result = '';
        foreach ($split as $value) {
            if (substr($value, 0, 5) === '<loop') {
                /** @var Loops\LoopInterface $loopObj */
                $loopObj = $this->loops[$nodeName];
                $result .= $loopObj
                    ->setTemplater($this)
                    ->setTemplate(
                        $this->templates[$tplName]['file']
                        , $value
                    )
                    ->setVars($this->vars)
                    ->__invoke();
            } else {
                $result .= $value;
            }
        }

        return $result;
    }
}