<?php
namespace Buckshot\Loops;
use Buckshot\Template;

interface LoopInterface
{
    public function getLoopName():string;
    public function setTemplater(Template $template):self;
    public function setTemplate(string $name, string $text):self;
    public function setVars(array $vars):self;
    public function __invoke():string;
}