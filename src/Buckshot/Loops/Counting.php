<?php
namespace Buckshot\Loops;

use Buckshot\Exceptions\RuntimeException;
use Buckshot\Template;

class Counting implements LoopInterface
{
    protected $templateFile;
    protected $templateText;
    protected $vars = [];
    const VARNAME_REGEXP = '/<var_name="\.\.\/([\w0-9_]+)">/';
    const LAST_NUMBER_REGEXP_SPLIT = '/(<if name="LastNumber">(|.|\s)+<\/if>)/';
    const LAST_NUMBER_REGEXP = '/<if name="LastNumber">((|.|\s)+)<\/if>/';
    /**
     * @var Template
     */
    protected $templater;

    public function getLoopName():string
    {
        return (new \ReflectionClass($this))->getShortName();
    }

    public function setTemplater(Template $template):LoopInterface
    {
        $this->templater = $template;

        return $this;
    }

    public function setTemplate(string $file, string $text):LoopInterface
    {
        $this->templateFile = $file;
        $this->templateText = $text;

        return $this;
    }

    public function setVars(array $vars):LoopInterface
    {
        $this->vars = $vars;

        return $this;
    }

    public function __invoke():string
    {
        $loopName = $this->getLoopName();
        $result = str_replace('</loop>', '', $this->templateText);
        $result = str_replace('<loop name="' . $loopName . '">', '', $result);
        $matches = [];
        if (false === preg_match(static::VARNAME_REGEXP, $result, $matches)) {
            throw new RuntimeException('Required var_name is not defined in ' . $loopName . ' in template ' . $this->templateFile);
        }
        $varName = $matches[1];
        if (!isset($this->vars[$varName])) {
            throw new RuntimeException('Undefined variable ' . $varName . ' in template "' . $this->templateFile . '"');
        }
        $varValue = (int)$this->vars[$varName];
        $result = preg_replace(static::VARNAME_REGEXP, $varValue, $result, 1);
        list($result, $subTemplate) = $this->prepareLastNumberSubTemplate($result, $loopName);
        $return = '';
        for ($i = 1; $i <= $varValue; $i++) {
            $row = $result;
            if ($i === $varValue) {
                $row = str_replace('[%SubTemplate%]', $subTemplate, $row);
            } else {
                $row = str_replace('[%SubTemplate%]', '', $row);
            }
            $return .= str_replace('<var name="Number">', $i, $row);
        }

        return $return;
    }

    protected function prepareLastNumberSubTemplate($result, $loopName)
    {
        $split = preg_split(
            static::LAST_NUMBER_REGEXP_SPLIT
            , $result
            , 2
            , PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
        $result = '';
        foreach ($split as $item) {
            if (substr($item, 0, 22) === '<if name="LastNumber">') {
                $matches = [];
                if (false !== preg_match(static::LAST_NUMBER_REGEXP, $item, $matches)) {
                    $subTemplate = $matches[1];
                    $subTemplate = $this->parseSubTemplate($loopName, $subTemplate);
                    $result .= '[%SubTemplate%]';
                } else {
                    $item = $this->parseSubTemplate($loopName, $item);
                    $result .= $item;
                }
            } else {
                $item = $this->parseSubTemplate($loopName, $item);
                $result .= $item;
            }
        }

        return array($result, $subTemplate);
    }

    protected function parseSubTemplate($loopName, $subTemplate)
    {
        $subTemplateName = $loopName . microtime() . random_int(0, 1000);
        $result = $this->templater
            ->subLoad($subTemplateName, $subTemplate, $this->templateFile)
            ->parse($subTemplateName)
            ->get($subTemplateName);

        return $result;
    }
}